from csv import reader, writer
from sys import argv


petateados = dict() #Diccionario en blanco

with open(argv[1],'r') as file: #Abre archivo (primer argumento)
    for row in reader(file): #Para cada linea en el archivo
        #Agrega la linea al diccionario en la fecha de defunción (row[12])
        if row[12] in petateados: 
            petateados[row[12]].append(row)
        else:
            petateados[row[12]]=list()
            petateados[row[12]].append(row)

for date in petateados.keys(): #Para cada fecha en el diccionario
    f = open('defunciones_'+date+'.csv','w+') #Crea un archivo en blanco
    #Imprime el conteniodo del diccionario en esa fecha al archivo
    write = writer(f) 
    write.writerows(petateados[date])
    f.close() #cierra el archivo
