## Datos sobre casos COVID 19 en la ciudad de Tijuana B.C.

### Estrucutura de los archivos CSV:

- FECHA_ACTUALIZACION
- ID_REGISTRO
- ORIGEN
- SECTOR
- ENTIDAD_UM
- SEXO
- ENTIDAD_NAC
- ENTIDAD_RES
- MUNICIPIO_RES
- TIPO_PACIENTE
- FECHA_INGRESO
- FECHA_SINTOMAS
- FECHA_DEF
- INTUBADO
- NEUMONIA
- EDAD
- NACIONALIDAD
- EMBARAZO
- HABLA_LENGUA_INDIG
- DIABETES
- EPOC
- ASMA
- INMUSUPR
- HIPERTENSION
- OTRA_COM
- CARDIOVASCULAR
- OBESIDAD
- RENAL_CRONICA
- TABAQUISMO
- OTRO_CASO
- RESULTADO
- MIGRANTE
- PAIS_NACIONALIDAD
- PAIS_ORIGEN
- UCI



Información extraída de [Datos Abiertos Bases Históricos](https://www.gob.mx/salud/documentos/datos-abiertos-bases-historicas-direccion-general-de-epidemiologia)

Autor: Secretaría de Salud



---

  Tange, O. (2020, July 22). GNU Parallel 20200722 ('Privacy Shield').
  Zenodo. https://doi.org/10.5281/zenodo.3956817
